/* eslint-disable react/jsx-filename-extension */
/* eslint-disable linebreak-style */
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, TouchableOpacity } from 'react-native';
import SplashScreen from '../screen/SplashScreen';
import LoginScreen from '../screen/LoginScreen';
import BottomRouter from './BottomRouter';
import APIDetailScreen from '../screen/APIDetailScreen';
import Channel from '../screen/Channel/Channel';
import InputChannel from '../screen/Channel/InputChannel';
import UpdateChannelScreen from '../screen/Channel/Update';
import ChartScreen from '../screen/ChartScreen';
import AddChartScreen from '../screen/AddChartScreen';
import Button from '../components/Button';
import { colorPrimary } from '../styles/colors';

const Stack = createStackNavigator();

const Router = (props) => (
  <Stack.Navigator initialRouteName="Splash">
    <Stack.Screen
      name="Splash"
      component={SplashScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="APIDetailScreen"
      component={APIDetailScreen}
      options={{
        title: 'Detail API',
      }}
    />
    <Stack.Screen
      name="Home"
      component={BottomRouter}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Channel"
      component={Channel}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="TambahChannel"
      component={InputChannel}
      options={{
        title: 'Tambah Channel',
      }}
    />
    <Stack.Screen
      name="UpdateChannel"
      component={UpdateChannelScreen}
      options={{
        title: 'Update Channel',
      }}
    />
    <Stack.Screen
      name="Chart"
      component={ChartScreen}
      options={({ navigation, route }) => ({
        title: 'Chart Container',
        headerTitle: 'Chart Channel',
        headerRight: () => (
          <Button style={styles.button}
            onPress={() => {
              console.log('routes', route.params);
              navigation.navigate('AddChartScreen', {
                id: route.params.id,
                apiKey: route.params.apiKey,
              });
            }}
            title="Tambah"
            color= "#000"
          />
        ),
      })}
    />
    <Stack.Screen
      name="AddChartScreen"
      component={AddChartScreen}
      options={{ title: 'Tambah Data' }}
    />
  </Stack.Navigator>
);

export default Router;

const styles = StyleSheet.create({
  button: {
    backgroundColor: colorPrimary,
    height: 50,
    width: 90,
  },
});