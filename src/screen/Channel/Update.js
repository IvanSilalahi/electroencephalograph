/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect } from 'react';
import {
  Alert,
  StyleSheet, View,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Button from '../../components/Button';
import Gap from '../../components/Gap';
import NewItem from '../../components/NewItem';
import { colorDanger } from '../../styles/colors';
import {
  ASGet, ASSet, showError, showSuccess,
} from '../../utils';

const UpdateChannelScreen = ({ route, navigation }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState('');
  const [api, setApi] = useState('');
  const [apiKey, setApiKey] = useState('');
  // const [id, setId] = useState(0);
  const [index, setIndex] = useState(0);
  const [channelList, setChannelList] = useState([]);

  const saveData = async () => {
    setIsLoading(true);

    const data = {
      id: route.params.id, name, api, apiKey,
    };
    const currentData = [...channelList];
    currentData[index] = data;

    await ASSet('channel', JSON.stringify(currentData))
      .then(() => {
        setChannelList(currentData);
        showSuccess('Data berhasil disimpan');
        navigation.navigate('Channel');
      })
      .catch((e) => {
        console.log('Update Data : ', e);
        showError(`Data gagal disimpan, dengan pesan: ${e.message}`);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const deleteData = async () => {
    Alert.alert('Hapus Channel', 'Apakah anda yakin ingin menghapus channel ini?', [
      {
        text: 'Ya',
        onPress: async () => {
          setIsLoading(true);

          const currentData = [...channelList];
          currentData.splice(index, 1);

          await ASSet('channel', JSON.stringify(currentData))
            .then(() => {
              setChannelList(currentData);
              showSuccess('Data berhasil dihapus');
              navigation.navigate('Channel');
            })
            .catch((e) => {
              console.log('Update Data : ', e);
              showError(`Data gagal dihapus, dengan pesan: ${e.message}`);
            })
            .finally(() => {
              setIsLoading(false);
            });
        },
      },
      {
        text: 'Tidak',
        style: 'cancel',
      },
    ]);
  };

  useEffect(async () => {
    await ASGet('channel').then((response) => {
      if (response) {
        const data = JSON.parse(response);
        for (const i in data) {
          if (data[i].id === route.params.id) {
            setName(data[i].name);
            setApi(data[i].api);
            setApiKey(data[i].apiKey);
            setIndex(i);
            break;
          }
        }

        setChannelList(JSON.parse(response));
      }

      //   setId(route.params.id);
      //   setName(route.params.name);
      //   setApi(route.params.api);
    });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <Gap height={20} />
        <NewItem
          label="Nama Channel"
          value={name}
          onChangeText={(dataNama) => {
            setName(dataNama);
          }}
        />
        <Gap height={30} />
        <NewItem
          label="Alamat API"
          value={api}
          onChangeText={(dataAPI) => {
            setApi(dataAPI);
          }}
        />
        <Gap height={30} />
        <NewItem
          label="API Key"
          value={apiKey}
          onChangeText={(value) => {
            setApiKey(value);
          }}
        />
        <Gap height={30} />
        <Button
          title="Simpan"
          onPress={saveData}
          isLoading={isLoading}
        />
      </View>
      <Button
        title="Hapus"
        onPress={deleteData}
        isLoading={isLoading}
        style={styles.buttonDelete}
      />
    </SafeAreaView>

  );
};

export default UpdateChannelScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  content: {
    width: '100%',
    alignItems: 'center',
  },
  buttonDelete: {
    backgroundColor: colorDanger,
  },
});
